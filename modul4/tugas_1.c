#include <stdio.h>

int main()
{

    int nilais[2][3];
    int ratios[3];

    printf("Nilai Boruto \n");
    for (int i = 0; i < 3; i++)
    {
        printf("Ujian Progdas ke-%d = ", i + 1);
        scanf("%d", &nilais[0][i]);
    }

    printf("Nilai Sarada \n");
    for (int i = 0; i < 3; i++)
    {
        printf("Ujian Progdas ke-%d = ", i + 1);
        scanf("%d", &nilais[1][i]);
    }

    for (int i = 0; i < 3; i++)
    {
        ratios[i] = (nilais[0][i] > nilais[1][i]) ? 1 : 0;
    }

    puts("");

    printf("Perbandingan nilai \n");
    for (int i = 0; i < 3; i++)
    {
        printf("Hasil ke-%d = %d \n", i + 1, ratios[i]);
    }

    return 0;
}
