#include <stdio.h>

int main()
{

    int length_choice;

    printf("Pilih ordo matrix\n");
    printf("2x2 inputkan 2 | 3x3 inputkan 3\n");
    printf("Ordo = ");
    scanf("%d", &length_choice);

    if (length_choice != 2 && length_choice != 3)
    {
        printf("Inputan salah");
        return 1;
    }

    int my_matrix[length_choice][length_choice];

    for (int i = 0; i < length_choice; i++)
    {
        for (int j = 0; j < length_choice; j++)
        {
            printf("Masukkan angka pada indeks [%d][%d] = ", i, j);
            scanf("%d", &my_matrix[i][j]);
            puts("");
        }
    }

    int largest, smallest;
    largest = smallest = my_matrix[0][0];

    for (int i = 0; i < length_choice; i++)
    {
        for (int j = 0; j < length_choice; j++)
        {
            printf("%d \t", my_matrix[i][j]);
            if (largest < my_matrix[i][j])
                largest = my_matrix[i][j];

            if (smallest > my_matrix[i][j])
                smallest = my_matrix[i][j];
        }
        puts("");
    }

    printf("\nTerkecil = %d \t Terbesar = %d \n", smallest, largest);

    return 0;
}