/**

Volume Balok            = plt
Luas Permukaan Balok    = 2 x ( (p x l) + (p x t) + (l x t) )

Volume Kubus            = s^3
Luas Permukaan Kubus    = 6 x (s * s)

Volume Tabung           = pi * r^2 * t
Luas Permukaan Tabung   = 2 * pi * r (r+t)

**/

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>

typedef struct balok
{
    float p, l, t;
} Balok;

float volumeBalok(Balok *);
float luasPermukaanBalok(Balok *);

typedef struct kubus
{
    float s;
} Kubus;

float volumeKubus(Kubus *);
float luasPermukaanKubus(Kubus *);

typedef struct tabung
{
    float r, t, PI;
} Tabung;

float volumeTabung(Tabung *);
float luasPermukaanTabung(Tabung *);

int main()
{

    int choice;

    do
    {
        puts("\n");
        printf("Hitung luas permukaan dan volume \n");
        printf("1. Balok \n");
        printf("2. Kubus \n");
        printf("3. Tabung \n\n");
        printf("4. Keluar \n\n");
        printf("Masukkan Pilihan [1,2,3,4] : ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            puts("\n");
            printf("Hitung luas permukaan dan volume balok \n");

            Balok balok;
            printf("Panjang Balok (cm): ");
            scanf("%f", &(balok.p));
            printf("Lebar Balok (cm): ");
            scanf("%f", &(balok.l));
            printf("Tinggi Balok (cm): ");
            scanf("%f", &(balok.t));

            puts("\n");
            printf("Volume Balok: %.2f cm^3", volumeBalok(&balok));
            puts("\n");
            printf("Luas Permukaan Balok: %.2f cm^2", luasPermukaanBalok(&balok));
            break;

        case 2:

            puts("\n");
            printf("Hitung luas permukaan dan volume kubus \n");

            Kubus kubus;
            printf("Panjang sisi kubus (cm): ");
            scanf("%f", &(kubus.s));

            puts("\n");
            printf("Volume Kubus: %.2f cm^3", volumeKubus(&kubus));
            puts("\n");
            printf("Luas Permukaan Kubus: %.2f cm^2", luasPermukaanKubus(&kubus));
            break;

        case 3:

            puts("\n");
            printf("Hitung luas permukaan dan volume tabung \n");

            Tabung tabung;
            printf("Jari-jari tabung (cm): ");
            scanf("%f", &(tabung.r));
            printf("Tinggi talok (cm): ");
            scanf("%f", &(tabung.t));
            tabung.PI = ((int)tabung.r % 7) == 0 ? 22.0 / 7.0 : M_PI;

            puts("\n");
            printf("Volume tabung: %.2f cm^3", volumeTabung(&tabung));
            puts("\n");
            printf("Luas Permukaan tabung: %.2f cm^2", luasPermukaanTabung(&tabung));

            break;

        default:
            printf("Pilihan tidak valid");
            break;
        }

    } while (choice != 4);

    return 0;
}

float volumeBalok(Balok *balok)
{
    return balok->p * balok->l * balok->t;
}

float luasPermukaanBalok(Balok *balok)
{
    return 2 * ((balok->p * balok->l) + (balok->p * balok->t) + (balok->l * balok->t));
}

float volumeKubus(Kubus *kubus)
{
    return pow(kubus->s, 3);
}

float luasPermukaanKubus(Kubus *kubus)
{
    return 6 * pow(kubus->s, 2);
}

float volumeTabung(Tabung *tabung)
{
    return tabung->PI * pow(tabung->r, 2) * tabung->t;
}

float luasPermukaanTabung(Tabung *tabung)
{
    return 2 * tabung->PI * tabung->r * (tabung->r + tabung->t);
}
