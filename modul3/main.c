#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct Menu
{
    int id;
    char name[50];
    unsigned int price;
} menu;

typedef struct Order
{
    menu *menu;
    unsigned int quantity;
} order;

void print_lines()
{
    puts("");
    printf("-----------------------------------------");
    puts("");
}

char *format_rupiah_currency(int value)
{
    char arrayValue[50];
    sprintf(arrayValue, "%d", value);
    char *formatted_value = malloc(50);
    strcat(formatted_value, "Rp. ");
    strcat(formatted_value, arrayValue);
    strcat(formatted_value, ",00");
    return formatted_value;
}

char my_getch()
{
    char c;
    char t;

    scanf("%c", &c);
    scanf("%c", &t);
    return (c);
}

int main()
{

    int checker = 0;

    do
    {
        const int MENU_SIZE = 3;
        menu menus[MENU_SIZE];
        unsigned short int item_count;

        menus[0].id = 1;
        strcpy(menus[0].name, "Yellow Rawon");
        menus[0].price = 99000;

        menus[1].id = 2;
        strcpy(menus[1].name, "Black Soto");
        menus[1].price = 102000;

        menus[2].id = 3;
        strcpy(menus[2].name, "Clear Gule");
        menus[2].price = 99000;

        puts("");
        printf("🍴 ====== 🍴 Resto Keminggris 🍴 ====== 🍴");
        puts("\n");

        printf("Choose your foods 🍲🥣🍛");
        puts("\n");

        printf("No. \t Name \t\t Price \n");
        for (int i = 0; i < MENU_SIZE; i++)
        {
            printf("%d \t %s \t %s\n", menus[i].id, menus[i].name, format_rupiah_currency(menus[i].price));
        }

        puts("");
        printf("How many menu(s) will you order? [ex: 2] ");
        scanf("%d", &item_count);

        if (item_count > MENU_SIZE)
        {
            char c;
            printf("We only have %d menu", MENU_SIZE);
            checker = 1;
            c = my_getch();
            system("clear");
        }
        else
        {
            print_lines();

            order order[item_count];

            int h = 0;
            int j = 1;
            unsigned short int id;
            while (item_count != 0)
            {
                print_lines();
                printf("Choose your food %d: ", j);
                scanf("%d", &id);
                switch (id)
                {
                case 1:
                    order[h].menu = &menus[0];
                    break;
                case 2:
                    order[h].menu = &menus[1];
                    break;
                case 3:
                    order[h].menu = &menus[2];
                    break;
                default:
                    printf("Menu is not valid");
                    checker = 1;
                }
                if (checker)
                    exit(EXIT_FAILURE);
                printf("You choose %s\n", order[h].menu->name);
                printf("How many %s do you want? ", order[h].menu->name);
                scanf("%d", &order[h].quantity);
                printf("QTY: %d %s", order[h].quantity, order[h].menu->name);
                puts("");
                printf("Total: %s", format_rupiah_currency(order[h].menu->price * order[h].quantity));
                ++j;
                --item_count;
                ++h;
            }

            unsigned int total_price = 0;
            unsigned int cash = 0;
            int change = 0;
            int ORDER_SIZE = sizeof(order) / sizeof(order[0]);
            for (int t = 0; t < ORDER_SIZE; t++)
            {
                total_price += order[t].menu->price * order[t].quantity;
            }
            print_lines();

            printf("Grand Total\t: %s", format_rupiah_currency(total_price));
            puts("");

            printf("Tunai\t\t: Rp. ");
            scanf("%u", &cash);
            puts("");

            if (cash < total_price)
                printf("Not enough money\n");
            else
            {
                change = cash - total_price;
                printf("Kembalian\t: %s", format_rupiah_currency(change));
                print_lines();
            }

            char s;
            printf("Start from the beginning? [Y/N] ");
            scanf(" %c", &s);
            if (toupper(s) == 'Y')
                checker = 1;
            else
                checker = 0;
        }
    } while (checker);

    return 0;
}
