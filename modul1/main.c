#include <stdio.h>

int main()
{
    float averageScore, progdasScore,
        calculusScore, ptiScore, orkomScore;

    printf("Masukkan nilai pemrograman dasar:");
    scanf("%f", &progdasScore);
    printf("Masukkan nilai kalkulus:");
    scanf("%f", &calculusScore);
    printf("Masukkan nilai PTI:");
    scanf("%f", &ptiScore);
    printf("Masukkan nilai OrKom:");
    scanf("%f", &orkomScore);

    averageScore = (progdasScore + calculusScore + ptiScore + orkomScore) / 4;
    printf("Nilai rata - rata: %.2f \n", averageScore);
    if (averageScore >= 75)
    {
        printf("Selamat anda lulus");
    }
    else
    {
        printf("Sayang sekali, Anda tidak lulus");
    }

    return 0;
}