#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    char id[11];
    char name[50];
    char clazz[20];
    char teacher_name[50];
} student;

void clear_output();

char my_getch();

int main()
{

    const int STUDENT_MAX = 99;
    int student_count = 0;

    int repeat = 0;
    student students[STUDENT_MAX];
    int choice;

    do
    {
        clear_output();
        printf("=== SI ===\n");
        printf("1. Insert Data\n2. Show Data\n3. Update Data\n4. Delete Data\n5. Search Data\n6. Exit\n");
        printf("Choose [1-6] = ");
        scanf("%d", &choice);

        char temp_id[11];
        int duplicate = 0;
        int exist = 0;

        switch (choice)
        {
        case 1:
            clear_output();
            printf("==👨== Insert new data ==👨==\n");
            printf("Student ID\t: ");
            scanf("%s", &temp_id);
            for (int i = 0; i < student_count; i++)
            {
                if (!strcmp(students[i].id, temp_id))
                    duplicate = 1;
            }
            if (strlen(temp_id) > 10)
            {
                printf("══════════════════════════════\n");
                printf("ID max length is 10 characters\n");
                printf("══════════════════════════════\n");
                printf("Press enter to return to menu\n");
                repeat = 1;
                my_getch();
                break;
            }
            if (duplicate)
            {
                printf("══════════════════\n");
                printf("ID already exists\n");
                printf("══════════════════\n");
                printf("Press enter to return to menu\n");
                repeat = 1;
                my_getch();
                break;
            }
            else
            {
                strcpy(students[student_count].id, temp_id);

                printf("Name \t\t= ");
                scanf(" %[^\n]s", &(students[student_count].name));

                printf("Class \t\t= ");
                scanf(" %[^\n]s", &(students[student_count].clazz));

                printf("Teacher \t= ");
                scanf(" %[^\n]s", &(students[student_count].teacher_name));

                student_count++;
                printf("═════════════\n");
                printf("Data Inserted\n");
                printf("═════════════\n");
            }
            printf("Press enter to return to menu\n");
            repeat = 1;
            my_getch();
            break;
        case 2:
            clear_output();
            printf("╔═══════════════════╗\n");
            printf("‖   List Students   ‖\n");
            printf("╚═══════════════════╝\n");
            if (student_count)
            {
                for (int i = 0; i < student_count; i++)
                {
                    printf("══ %d ══\n", i + 1);
                    printf("ID\t = %s\n", students[i].id);
                    printf("Name\t = %s\n", students[i].name);
                    printf("Class\t = %s\n", students[i].clazz);
                    printf("Teacher\t = %s\n", students[i].teacher_name);
                    printf("═══════════════════\n");
                }
            }
            else
            {
                printf("╔═════════════════╗\n");
                printf("‖No data available‖\n");
                printf("╚═════════════════╝\n");
            }
            printf("Press enter to return to menu\n");
            repeat = 1;
            my_getch();
            break;
        case 3:
            clear_output();
            printf("╔═══════════════════╗\n");
            printf("‖    Update Data    ‖\n");
            printf("╚═══════════════════╝\n");
            printf("Enter ID: ");
            scanf("%s", &temp_id);
            exist = 0;
            int index;
            for (int i = 0; i < student_count; ++i)
            {
                if (!strcmp(temp_id, students[i].id))
                {
                    exist = 1;
                    index = i;
                    break;
                }
            }
            if (exist)
            {
                printf("══════════════════════════\n");
                printf("ID exists, insert new data\n");
                printf("══════════════════════════\n");
                printf("Name \t: ");
                scanf(" %[^\n]s", &(students[index].name));
                printf("Class \t: ");
                scanf(" %[^\n]s", &(students[index].clazz));
                printf("Teacher : ");
                scanf(" %[^\n]s", &(students[index].teacher_name));
                printf("═════════════\n");
                printf("Data Updated\n");
                printf("═════════════\n");
            }
            else
            {
                printf("════════════\n");
                printf("No ID exists\n");
                printf("════════════\n");
            }
            printf("Press enter to return to menu\n");
            repeat = 1;
            my_getch();
            break;
        case 4:
            clear_output();
            printf("╔═══════════════════╗\n");
            printf("‖    Delete Data    ‖\n");
            printf("╚═══════════════════╝\n");
            printf("Enter ID: ");
            scanf("%s", &temp_id);
            exist = 0;
            for (int i = 0; i < student_count; ++i)
            {
                if (!strcmp(temp_id, students[i].id))
                {
                    exist = 1;
                    index = i;
                    break;
                }
            }
            if (exist)
            {
                for (int i = index; i < student_count - 1; i++)
                {
                    students[i] = students[i + 1];
                    strcpy(students[i + 1].id, "\000");
                    strcpy(students[i + 1].name, "\000");
                    strcpy(students[i + 1].clazz, "\000");
                    strcpy(students[i + 1].teacher_name, "\000");
                }
                student_count--;
                printf("═════════════\n");
                printf("Data Deleted\n");
                printf("═════════════\n");
            }
            else
            {
                printf("════════════\n");
                printf("No ID exists\n");
                printf("════════════\n");
            }
            printf("Press enter to return to menu\n");
            repeat = 1;
            my_getch();
            break;
        case 5:
            clear_output();
            printf("╔═══════════════════╗\n");
            printf("‖    Search Data    ‖\n");
            printf("╚═══════════════════╝\n");
            printf("Enter ID: ");
            scanf("%s", &temp_id);
            exist = 0;
            for (int i = 0; i < student_count; ++i)
            {
                if (!strcmp(temp_id, students[i].id))
                {
                    exist = 1;
                    index = i;
                    break;
                }
            }
            if (exist)
            {
                printf("═══════════════════\n");
                printf("ID\t= %s\n", students[index].id);
                printf("Name\t= %s\n", students[index].name);
                printf("Class\t= %s\n", students[index].clazz);
                printf("Teacher\t= %s\n", students[index].teacher_name);
                printf("═══════════════════\n");
            }
            else
            {
                printf("════════════\n");
                printf("No ID exists\n");
                printf("════════════\n");
            }
            printf("Press enter to return to menu\n");
            repeat = 1;
            my_getch();
            break;
        case 6:
            repeat = 0; break; // atau langsung return 0;
        default:
            printf("\nInput is not valid\n");
            printf("Press enter to return to menu\n");
            repeat = 1;
            my_getch();
            break;
        }
    } while (repeat);

    return 0;
}

void clear_output()
{
#ifdef _WIN32
    system("cls");
#endif
#ifdef __unix__
    system("clear");
#endif
}

char my_getch()
{
    char c;
    char t;

    scanf("%c", &c);
    scanf("%c", &t);
    return (c);
}